﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance = null;

    public Text instructionText;
    public Text numOfLifeText;
    public GameObject handAnim;
    public Button placeButton;
    public Button playerStartButton;

    public Button pauseButton;
    public Button resumeButton;
    public Button homeButton;
    public GameObject pauseMenu;

    public GameObject gameOver;
    public Button gameOverHomeButton;

    private void OnEnable() 
    {
        placeButton.gameObject.SetActive(false);
        playerStartButton.gameObject.SetActive(false);
        ShowGO(pauseMenu, false);
        ShowGO(gameOver.gameObject, false);
        pauseButton.onClick.AddListener(ShowPauseMenu);
        resumeButton.onClick.AddListener(ResumeGame);
        homeButton.onClick.AddListener(GoToMainScene);

        gameOverHomeButton.onClick.AddListener(GoToMainScene);
        placeButton.onClick.AddListener(StartGame);
        playerStartButton.onClick.AddListener(PlayerStart);
        GameEventManager.GameStart += GameEventManager_GameStart;
        GameEventManager.GameOver += GameEventManager_GameOver;
    }

    private void GameEventManager_GameOver()
    {
        ShowGO(gameOver.gameObject, true);
    }

    private void GoToMainScene()
    {
        SceneManager.LoadSceneAsync("gameManger");
    }

    private void ResumeGame()
    {
        ShowGO(pauseMenu, false);
        GameEventManager.OnGameResumed();
    }

    private void ShowPauseMenu()
    {
        ShowGO(pauseMenu, true);
        GameEventManager.OnGamePaused();
    }

    private void PlayerStart()
    {
        ShowText("Swipe Left Right to change the direction");
        Utils.Utils.ShowAndroidToastMessage("Swipe Left Right to change the direction");
        GlobalVariables.canPlayerStart = true;
    }

    private void StartGame()
    {
        GameEventManager.OnLevelInstantiate();
       
    }

    public void Awake()
    {
        
        Instance = this;
        

    }

    void Start()
    {

        ShowGO(handAnim, true);
        ShowText("Scan for a Plane.. ");
        Utils.Utils.ShowAndroidToastMessage("Scan for a Plane.. ");
    }


    public void ShowGO(GameObject go ,bool show)
    {
        if(go!=null)
        go.SetActive(show);
    }
    public void ShowText(string message)
    {
       
        instructionText.text = message;
    }
    private void GameEventManager_GameStart()
    {
       


    }
    void PlaceModel()
    {

    }
   
    void Update()
    {

        numOfLifeText.text = ""+GlobalVariables.numOfLives;

    }

    
}
 
