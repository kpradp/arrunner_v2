﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVariables : MonoBehaviour
{
    public static bool gamePaused;
    public static bool gameResumed;
    public static bool isPlaneDiscovered = false;
    public static bool isObjectPlaced = false;
    public static bool canPlayerStart = false;
    public static int numOfLives = 3;
    
}
