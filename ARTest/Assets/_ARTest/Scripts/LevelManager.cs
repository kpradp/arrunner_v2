﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance = null;
    public GameObject spawnerParent;
    public GameObject startTile;
    public List<GameObject> floorPrefabs;
    public GameObject playerRef;

    public GameObject spawnPrefab;

    GameObject spawnFloor;
    GameObject floorStart;
    private Queue<GameObject> spawnFloorQueue;
    public static event Action SpawnEvent, RemoveSpawnEvent;
    TKSwipeRecognizer recognizer;
    GameObject player;
    public int numberOfObjectSpawn;// = 4;
    
    public GameObject referencePoint;

    Vector3 lastPosition;
    float distanceTravelled = 0;
    public float movementSpeed;
    public UIManager uiManager;
    private void OnEnable()
    {

        GameEventManager.SpawnEvent += GroundSpawner_SpawnEvent;
        GameEventManager.RemoveSpawnEvent += GroundSpawner_RemoveSpawnEvent;
        GameEventManager.PlayerDead += GameEventManager_PlayerDead;
        GameEventManager.GameOver += GameEventManager_GameOver;
        GameEventManager.LevelInstantiateEvent += GameEventManager_LevelInstantiateEvent;
    }

   

    private void Awake()
    {
        Instance = this;
        GlobalVariables.numOfLives = 3;
        uiManager = UIManager.Instance;
        recognizer = new TKSwipeRecognizer();
        recognizer.gestureRecognizedEvent += Recognizer_GestureRecognizedEvent;
        TouchKit.addGestureRecognizer(recognizer);
    }

    void Start()
    {
        spawnFloorQueue = new Queue<GameObject>(numberOfObjectSpawn);
       // GameEventManager.OnGameStart();
       // GameEventManager_LevelInstantiateEvent(); // TODO DELETE
    }

    private void GameEventManager_GameOver()
    {
        //show game over scene
        // restart Option
    }

    private void GameEventManager_LevelInstantiateEvent()
    {
        InstantiateGround();
        distanceTravelled = 0;
        lastPosition = Vector3.zero;
      //  Debug.Log("Calling start");

    }

    public void InstantiateGround()
    {
        UIManager.Instance.ShowText("Click Play to start ur game..");
        Utils.Utils.ShowAndroidToastMessage("Click Play to start ur game..");
        UIManager.Instance.placeButton.gameObject.SetActive(false);
        UIManager.Instance.playerStartButton.gameObject.SetActive(true);
       // if (referencePoint != null)
        {
            referencePoint = GameObject.FindGameObjectWithTag("ReferencePoint");
          //  referencePoint.gameObject.SetActive(false);
        }
           
        
       // referencePoint.gameObject.SetActive(false);
        spawnFloor = Instantiate(spawnPrefab);//, referencePoint.transform.position, Quaternion.identity) as GameObject;
        spawnFloor.transform.position = referencePoint.transform.position;
        // spawnFloor.transform.rotation = referencePoint.transform.rotation;
       
       // spawnFloor.transform.position = spawnPrefab.transform.position;
        spawnFloor.transform.rotation = spawnPrefab.transform.rotation;
        floorStart = spawnFloor.transform.GetChild(0).gameObject;
        floorStart.SetActive(true);
        startTile = spawnFloor.transform.GetChild(0).gameObject;
        spawnFloor.transform.parent = spawnerParent.transform;
        playerRef = GameObject.FindGameObjectWithTag("Player");
        
        
       // Debug.Log(playerRef);
        Spawn();
       
    }
    private void GroundSpawner_SpawnEvent()
    { 
        Spawn(); 
    }
    private void GroundSpawner_RemoveSpawnEvent()
    {

        ReSpawnFromQueue();
        Debug.Log("removing old spawns ");
    }

    void ReSpawnFromQueue()
    {
        var c = spawnFloorQueue.Dequeue();
        c.transform.position = startTile.transform.GetChild(0).transform.position;
        c.transform.parent = spawnerParent.transform;
        spawnFloorQueue.Enqueue(c);
        if (floorStart.activeSelf) floorStart.SetActive(false);
    }

    void Spawn()
    {
        distanceTravelled = 0;
        for (int i = 0; i < numberOfObjectSpawn; i++)
        {
            var rand = UnityEngine.Random.Range(0, floorPrefabs.Count);
            var c = GameObject.Instantiate(floorPrefabs[rand]) as GameObject;
            c.transform.position = startTile.transform.GetChild(0).transform.position;
            c.transform.parent = spawnerParent.transform;
            startTile = c;
            
            spawnFloorQueue.Enqueue(c);

        }
 
    }

    private void Recognizer_GestureRecognizedEvent(TKSwipeRecognizer r)
    {
        switch (r.completedSwipeDirection)
        {
            case TKSwipeDirection.Left:
                playerRef.transform.Rotate(playerRef.transform.rotation.x, -90, playerRef.transform.rotation.z);
                break;

            case TKSwipeDirection.Right:
                playerRef.transform.Rotate(playerRef.transform.rotation.x, 90, playerRef.transform.rotation.z);
                break;
        }
     //   Debug.Log(r.completedSwipeDirection);
    }
    private void GameEventManager_PlayerDead()
    {
        foreach (var item in spawnFloorQueue)
        {
            Destroy(item);
        }
        spawnFloorQueue.Clear();
         
        //Destroy(spawnFloor);
        //Debug.Log("@##$$");
        GlobalVariables.canPlayerStart = false;
        distanceTravelled = 0;
        if (GlobalVariables.numOfLives > 0)
            GameEventManager.OnLevelInstantiate();
        else
            GameEventManager.OnGameOver();
    }

    void MovePlayer()
    {

        if (!playerRef) playerRef = GameObject.FindGameObjectWithTag("Player");
        
       
        playerRef.transform.position += playerRef.transform.forward * Time.deltaTime * (movementSpeed / 100);
        distanceTravelled += Vector3.Distance(playerRef.transform.position, lastPosition);
        lastPosition = playerRef.transform.position;
        //Debug.Log(distanceTravelled);
        //Debug.Log(playerRef.transform.position);
        if (distanceTravelled >= 1.4f)
        {
           
           
            GameEventManager.OnSpawn();
            GameEventManager.OnRemoveSpawn();
            distanceTravelled = 0;
        }
        if (playerRef.transform.position.y <= -1)
        {
            GlobalVariables.numOfLives--;
            distanceTravelled = 0;
            Destroy(playerRef.gameObject);
            
            GameEventManager.OnPlayerDead();
           
        }
         
    }
    // Update is called once per frame
    void Update()
    {
       
        if (GlobalVariables.canPlayerStart)
            MovePlayer();

      
    }
}
