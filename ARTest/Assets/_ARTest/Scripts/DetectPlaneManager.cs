﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;


#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif

public class DetectPlaneManager : MonoBehaviour
{

    public Camera FirstPersonCamera;
    private bool m_IsQuitting = false;
    public GameObject prefabObject;
    private const float k_ModelRotation = 180.0f;
    
    GameObject prefabGO;
    private List<DetectedPlane> m_DetectedPlanes = new List<DetectedPlane>();


    private void Awake()
    {
        
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _UpdateApplicationLifecycle();

        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        // Should not handle input if the player is pointing on UI.
        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            return;
        }
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }

        Session.GetTrackables<DetectedPlane>(m_DetectedPlanes, TrackableQueryFilter.All);

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (GlobalVariables.canPlayerStart) return;
        Vector2 touchPoint = Input.GetTouch(0).position;
        if (Frame.Raycast(touchPoint.x, touchPoint.y, raycastFilter, out hit))
        {
            if ((hit.Trackable is DetectedPlane) &&
                   Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                       hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            { 
                if (hit.Trackable is DetectedPlane)
                {
                    _ = hit.Trackable as DetectedPlane;
                    GlobalVariables.isPlaneDiscovered = true;
                  
                   
                    if (prefabGO ==null)
                    {
                        prefabGO = Instantiate(prefabObject, hit.Pose.position, hit.Pose.rotation) as GameObject;
                        var anchor = hit.Trackable.CreateAnchor(hit.Pose);
                        prefabGO.transform.parent = anchor.transform;
                        GlobalVariables.isObjectPlaced = true;
                        prefabGO.transform.position = hit.Pose.position;

                        UIManager.Instance.ShowText("Click Place button to place the level");
                        UIManager.Instance.placeButton.gameObject.SetActive(true);
                        
                        Utils.Utils.ShowAndroidToastMessage("Click Place button to place the level");
                    }
                    else
                    {
                        prefabGO.transform.position = hit.Pose.position;
                    }
                }
                
                
              
            }

        }
        

       
    }

    


    private void _UpdateApplicationLifecycle()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        // Only allow the screen to sleep when not tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
           Utils.Utils.ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            Utils.Utils.ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }

    private void _DoQuit()
    {
        Application.Quit();
    }

    
}
 
