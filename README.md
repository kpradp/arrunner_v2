# AR Runner

ARCore SDK is used with Unity 
Plane tracking is done using ARCore

** Works best with a Table top since plane scanning and level scale are set to a table size **

## Main Scene

There is a play button on the main screen click on which will load the Game scene

## Game Scene

* A hand animation is played indicating to scan a plane 
* On Successful scan user can click on the plane and place the marker
* Marker postion can be moved by clicking on appropriate place in the detected plane
* A place level button will be displayed to place the initial part of the level


The level is generated procedurally hence to avoid hassle the game view is set to potrait

Cliking on start will start the charcter to move on the generated level
Left and straight blocks are spawned automatically based on the player move distance
Falling from the blocks result in a life loss

![RefImage] (https://bitbucket.org/kpradp/arrunner_v2/raw/30c8de3b713cc0dde6152bbdfc46c3546f9e815f/image.jpeg)
## Controls

* Left swipe and right swipe for turning the character

* 3 lives are given and the game will end once over

## UI

* Minimal screens are used to avoid loops
* Pause screen can be accessed from the pause button
* Game over screen will be shown once the player life ends
* Display messages are shown in the top snack bar and also as android toast in the botton

## Managers

* Game Manager - Used to manage the game routine
* Game Event Manager - Used to manage the events in the game
* Level Manager - Responsoble for level spawns and tracking player distances
* Andy Model is used as a player with no animation



